#!/bin/bash

# Autocorrelation of the potential energy using weights
echo 5 | g_energy_d_mpibull_gshmc_development -f gshmc.edr

g_analyze_d_mpibull_gshmc_development -f energy.xvg -ac autocorr.xvg -nofftcorr -gshmc

rm *#
