#!/bin/bash

# MD
grompp_d_mpibull_gshmc_development -f md.mdp -n index.ndx -o md

mdrun_d_mpibull_gshmc_development -s md -o md -x md -e md -g md -c md

# MD - Two-stage integrator
grompp_d_mpibull_gshmc_development -f md_twos.mdp -n index.ndx -o twos

mdrun_d_mpibull_gshmc_development -s twos -o twos -x twos -e twos -g twos -c twos

# MD - Adaptive Integration Approach (AIA)
grompp_d_mpibull_gshmc_development -f aia.mdp -n index.ndx -o aia

mdrun_d_mpibull_gshmc_development -s aia -o aia -x aia -e aia -g aia -c aia

# HMC
grompp_d_mpibull_gshmc_development -f hmc.mdp -n index.ndx -o hmc

mdrun_d_mpibull_gshmc_development -s hmc -o hmc -x hmc -e hmc -g hmc -c hmc

# GSHMC
grompp_d_mpibull_gshmc_development -f gshmc.mdp -n index.ndx -o gshmc

mdrun_d_mpibull_gshmc_development -s gshmc -o gshmc -x gshmc -e gshmc -g gshmc -c gshmc

rm *#
